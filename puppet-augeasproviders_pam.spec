Autoreq: 0

Name:           puppet-augeasproviders_pam
Version:        1.0
Release:        5%{?dist}
Summary:        Puppetlabs augeasproviders_pam module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs augeasproviders_pam module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_pam/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_pam/
touch %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_pam/supporting_module

%files -n puppet-augeasproviders_pam
%{_datadir}/puppet/modules/augeasproviders_pam
%doc code/README.md

%changelog
* Mon Jan 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.0-5
- Rebased to #ccaf04a14fc53f8a0d787b7ad8764e74a1b142b1 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-4
- Bump release for disttag change

* Thu Apr 15 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-3
- use Autoreq to ensure that rpsec is not brought in as a dependency

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- fix requires on puppet-agent

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
-Initial release
